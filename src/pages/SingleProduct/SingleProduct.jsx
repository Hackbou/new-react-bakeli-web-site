import React from 'react';
import { Button, Container, IconButton } from '@mui/material';
import './singleProduct.scss';
import { Col, Row } from 'react-bootstrap';
import SelectMenue from '../../utils/mui/SelectMenue';
import SelectQuantite from '../../utils/mui/SelectQuantite';
import { Add, ShoppingCart } from '@mui/icons-material';

const Singleproduct = () => {
    return (
        <div>
            <Container className="my-5 pt-3">
                <Row>
                    <Col className="col-md-6 col-12">
                        <div className="singleProduct-img-mere">
                            <img src="assets/products/product_11.jpg" alt="" />
                        </div>
                        <div className="singleProduct-img-filles">
                            <div className="singleProduct-img-fille">
                                <img src="assets/products/product_17.jpg" alt="" />
                            </div>
                            <div className="singleProduct-img-fille">
                                <img src="assets/products/product_20.jpg" alt="" />
                            </div>
                            <div className="singleProduct-img-fille">
                                <img src="assets/products/product_19.jpg" alt="" />
                            </div>
                            <div className="singleProduct-img-fille">
                                <img src="assets/products/product_18.jpg" alt="" />
                            </div>
                        </div>
                    </Col>
                    <Col className="col-md-6 col-12">
                        <div className="singleProduct-body">
                            <h3 className="h2">Title of the Card</h3>
                            <span className="singleProduct-price">2500 FCFA</span>
                            <p className="paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque, sint. Ex sit magnam earum ut. Nesciunt modi placeat eum, nostrum mollitia omnis officiis unde ducimus a obcaecati beatae, excepturi voluptas?</p>
                            <SelectMenue />
                            <SelectQuantite Title="Quantite" />
                            <div className="singleProduct-body-bottom">
                                <IconButton className="singleProduct-body-icon" size="large">
                                    <ShoppingCart />
                                </IconButton>
                                <Button className="singleProduct-body-btn" size="large">
                                    <Add /> Ajouter des produits
                                </Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Singleproduct;
