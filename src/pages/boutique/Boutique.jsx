import './boutique.scss';
import React, { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Products } from '../../utils/api/Product';
import Cards from '../../utils/cardProduct/Card';

const Boutique = () => {

    const [query, setQuery] = useState('');

    return (
        <>
            <div bg="light" className="head">
                <Container className="py-3">
                    <Row>
                        <Col className='col-lg-8 col-md-8 col-sm-6 col-xs-12'>
                            <h3>Vetements</h3>
                        </Col>
                        <Col md={4} sm={6} xs={12} className="favorite-search ml-auto">
                            <input
                                className="form form-control"
                                type="text"
                                placeholder="Recherche..."
                                onChange={(e) => setQuery(e.target.value)}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>

            <div className="product py-2 mt-3">
                <Container className="my-5">

                    <Row>
                        {Products.filter((product) =>
                            product.libelle.toLowerCase().includes(query)
                        ).map(product => (
                            <Cards
                                key={product.id}
                                Image={product.image}
                                Title={product.libelle}
                                Price={product.price}
                            />
                        ))}
                    </Row>

                </Container>
            </div>

        </>
    );
}

export default Boutique;
