import React from 'react';
import { IconButton, Button } from '@mui/material';
import Modal from 'react-bootstrap/Modal';
import './modal.scss';
import { Add, Close, ShoppingCart } from '@mui/icons-material';
import { Link } from 'react-router-dom';


export default function ModalCard(props) {

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >

            <Modal.Body className="modal-body">
                <div className="modal-img">
                    <img src={props.Image} alt="" className="img" />
                    <span className="modal-price">{props.Price} fcfa</span>
                </div>
                <div className="modal-info">
                    <h3>{props.Title}</h3>
                    <p>{props.Description}</p>
                    <div className="modal-btn">
                        <IconButton className="modal-icon" size="large">
                            <ShoppingCart />
                        </IconButton>
                        <Link to="/singleProduct" style={{ textDecoration: "none" }}>
                            <Button className="modal-icon modal-add" size="large">
                                <Add /> Detail
                            </Button>
                        </Link>
                    </div>
                </div>
                <IconButton onClick={props.onHide} className="modal-close" size="large">
                    <Close />
                </IconButton >
            </Modal.Body>

        </Modal>
    );

}
