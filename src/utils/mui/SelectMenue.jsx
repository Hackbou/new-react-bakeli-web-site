import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function SelectMenue() {
    const [taille, setTaille] = React.useState('');

    const handleChange = (event) => {
        setTaille(event.target.value);
    };

    return (
        <div>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="demo-simple-select-standard-label">Taille</InputLabel>
                <Select
                    labelId="demo-simple-select-standard-label"
                    id="demo-simple-select-standard"
                    value={taille}
                    onChange={handleChange}
                    label="Taille"
                >
                    <MenuItem value="SM">
                        <em>SM</em>
                    </MenuItem>
                    <MenuItem value="LG">LG</MenuItem>
                    <MenuItem value="SM">XM</MenuItem>
                    <MenuItem value="XM">XS</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}
