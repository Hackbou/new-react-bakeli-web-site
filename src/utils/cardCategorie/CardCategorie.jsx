import React from 'react'
import { Card, Col } from 'react-bootstrap';
import './cardCategorie.scss';

export default function CardCategorie(props) {
    return (
        <Col className="mb-4 container-md-fluid container-sm-fluid container-xs-fluid" lg={6} md={12} sm={12} xs={12}>
            <Card className="categorie-card">
                <img src={props.Image} alt="" />
                <div className="categorie-card-body">
                    <h3>{props.Title}</h3>
                    <p>{props.Description}</p>
                </div>
            </Card>
        </Col>
    )
}
