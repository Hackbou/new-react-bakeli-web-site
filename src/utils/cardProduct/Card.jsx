import { Search, ShoppingCart, FavoriteBorder, Favorite } from '@mui/icons-material';
import React, { useState } from 'react';
import { Card, Col } from 'react-bootstrap';
import './card.scss';
import { IconButton } from '@mui/material';
import ModalCard from '../modals/ModalCard';

export default function Cards(props) {

    const [like, setLike] = useState('unlike');
    const [modalShow, setModalShow] = React.useState(false);

    const liked = () => {
        setLike('like');
    }

    const unliked = () => {
        setLike('unlike');
    }

    return (
        <Col className="mb-4" lg={4} md={6} sm={6} xs={12}>
            <Card className="card-product">
                <div className="card-img">
                    <Card.Img onClick={() => setModalShow(true)} className="img w-100" variant="top" src={props.Image} />
                </div>
                <div className="card-body">
                    <Card.Body>
                        <Card.Title className="title">{props.Title}</Card.Title>
                        <Card.Text className="des">
                            <div className="icons">
                                <IconButton className="icon" aria-label="delete" size="large">
                                    <ShoppingCart />
                                </IconButton>
                                <IconButton className="icon" aria-label="delete" size="large">
                                    <Search onClick={() => setModalShow(true)} />
                                </IconButton>
                                {
                                    like === 'unlike' && (
                                        <IconButton onClick={liked} className="icon" aria-label="delete" size="large">
                                            <FavoriteBorder />
                                        </IconButton>
                                    )
                                }

                                {
                                    like === 'like' && (
                                        <IconButton onClick={unliked} className="icon like" aria-label="delete" size="large">
                                            <Favorite />
                                        </IconButton>
                                    )
                                }
                            </div>

                            <div className="price">
                                <p>{props.Price} FCFa</p>
                            </div>
                        </Card.Text>
                    </Card.Body>
                </div>
            </Card>

            <ModalCard
                show={modalShow}
                onHide={() => setModalShow(false)}
                Image={props.Image}
                Price={props.Price}
                Title={props.Title}
                Description={props.Description}
            />

        </Col>
    )
}
