import { Home, FiberNew, Info, Storefront } from '@mui/icons-material';
// import { Menu } from '@mui/icons-material';
import React from 'react';
import { Link } from 'react-router-dom';
import './header.scss';

const Header = () => {

    // const [active, setActive] = useState(false);

    // const toggle = () => {
    //     setActive(!active);
    // }

    return (
        <div className="header-container">
            <header className="header-nav container">

                <div className="header-nav-left">
                    <h1 className="h1">CNS</h1>
                    <span>shop</span>
                </div>
                <nav className="header-nav-right">
                    <ul>
                        <li> <Link className="nav-link" exact to="/"> <Home className="nav-icon" /> <span>Home</span>  </Link> </li>
                        <li> <Link className="nav-link" exact to="/boutique"> <Storefront className="nav-icon" /> <span>Boutique</span> </Link> </li>
                        <li> <Link className="nav-link" exact to="/new"> <FiberNew className="nav-icon" /> <span>Nouveaute</span> </Link> </li>
                        <li> <Link className="nav-link" exact to="/about"> <Info className="nav-icon" /> <span>Contact</span> </Link> </li>
                    </ul>
                </nav>

            </header>
        </div>
    );
}

export default Header;
