import { FiberNew, Home, Info, Storefront } from '@mui/icons-material';
import React from 'react';
import { useState } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import './header2.scss';

const Header2 = () => {

    const [act, setActive] = useState('first');

    const first = () => {
        setActive('first');
    }

    const second = () => {
        setActive('second');
    }

    const third = () => {
        setActive('third');
    }

    return (
        <HeaderContainer>
            <div className="header">
                <div className="filter"></div>
                <div className="header-top">
                    {/* <div className="header-logo">
                        <h3>CNS</h3>
                        <span>shop</span>
                    </div>
                    <div className="header-card">
                        panier
                        <div className="header-card-icon">
                            <ShoppingCart />
                            <span>0</span>
                        </div>
                    </div> */}
                    <h1 className="h1">CNS</h1>
                    <span>shop</span>
                </div>

                <div className="header-box">
                    <div onClick={first} className="header-box-items">
                        <img src="assets/products/product_14.jpg" alt="" />
                    </div>
                    <div onClick={second} className="header-box-items">
                        <img src="assets/products/product_18.jpg" alt="" />
                    </div>
                    <div onClick={third} className="header-box-items">
                        <img src="assets/products/product_20.jpg" alt="" />
                    </div>
                </div>

                <div className="header-bottom">
                    <ul>
                        <NavLink exact to="/"> <li><Home /> </li> </NavLink>
                        <NavLink exact to="/boutique"> <li><Storefront /> </li></NavLink>
                        <NavLink exact to="/new"> <li><FiberNew /> </li></NavLink>
                        <NavLink exact to="/about"> <li><Info /> </li></NavLink>
                    </ul>
                </div>
            </div>
            <div className="header-right">
                {
                    act === 'first' && (
                        <img src="assets/products/product_14.jpg" alt="" />
                    )
                }
                {
                    act === 'second' && (
                        <img src="assets/products/product_18.jpg" alt="" />
                    )
                }
                {
                    act === 'third' && (
                        <img src="assets/products/product_20.jpg" alt="" />
                    )
                }
            </div>
        </HeaderContainer>
    );
}

const HeaderContainer = styled.div`
    width: 100%;
    height: 550px;
    position: relative;
    overflow: hidden;
    margin-bottom: 100px
`;

export default Header2;
