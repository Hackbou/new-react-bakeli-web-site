import React from "react";
import { MDBFooter } from "mdbreact";
import './footer.scss';
import { Facebook, Twitter, WhatsApp, YouTube } from "@mui/icons-material";

const Footer = () => {
    return (
        <MDBFooter id="footer" className="footer-1 footer">
            <div className="main-footer widgets-dark typo-light">
                <div className="container">
                    <div className="row">

                        <div className="col-xs-12 col-sm-6 col-md-3">
                            <div className="widget subscribe no-box">
                                <h5 className="widget-title">COMPANY NAME<span></span></h5>
                                <p>About the company, little description will goes here.. </p>
                            </div>
                        </div>


                        <div className="col-xs-12 col-sm-6 col-md-3">
                            <div className="widget no-box">
                                <h5 className="widget-title">Quick Links<span></span></h5>
                                <ul className="thumbnail-widget">
                                    <li>
                                        <div className="thumb-content"><a href="#.">&nbsp;Get Started</a></div>
                                    </li>
                                    <li>
                                        <div className="thumb-content"><a href="#.">&nbsp;Top Leaders</a></div>
                                    </li>
                                </ul>
                            </div>
                        </div>



                        <div className="col-xs-12 col-sm-6 col-md-3">
                            <div className="widget no-box">
                                <h5 className="widget-title">Follow up<span></span></h5>
                                <a href="#"> <Facebook /> </a>
                                <a href="#"> <Twitter /> </a>
                                <a href="#"> <WhatsApp /> </a>
                                <a href="#"> <YouTube /> </a>
                            </div>
                        </div>
                        <br />
                        <br />


                        <div className="col-xs-12 col-sm-6 col-md-3">
                            <div className="widget no-box">
                                <h5 className="widget-title">Contact Us<span></span></h5>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                                <div className="emailfield">
                                    <form>
                                        <input type="text" name="email" value="Email" />
                                        <input name="uri" type="hidden" value="arabiantheme" />
                                        <input name="loc" type="hidden" value="en_US" />
                                        <input className="submitbutton ripplelink" type="submit" value="Subscribe" />
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="footer-copyright">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <p>Copyright Design Sherif Hamdy © 2019. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MDBFooter>
    );
}

export default Footer;