import React, { useState } from 'react';
import './favorite.scss';
import { Container, Row } from 'react-bootstrap';
import Cards from '../../utils/cardProduct/Card';
import { Products } from '../../utils/api/Product';
import { Search } from '@mui/icons-material';

const Favorite = () => {

    const [query, setQuery] = useState("");
    const [active, setActive] = useState(false);

    const toggle = () => {
        setActive(!active);
    }

    // console.log(Products.filter(product => product.libelle.toLowerCase().includes(query)));

    return (
        <div className="favorite py-2 mt-3">
            <Container className="my-5">
                <Row className="mb-5">
                    <div className="favorite-top">
                        <div className="favorite-title">
                            <h3>Favorites</h3>
                        </div>
                        <div className={'favorite-search' + (active ? ' favorite-search-active' : '')}>
                            <Search onClick={toggle} className="icon" />
                            <input onChange={(e) => setQuery(e.target.value)} type="text" className="search-bar" placeholder="Recherche..." />
                        </div>
                    </div>
                </Row>

                <Row>
                    {Products.filter((product) =>
                        product.libelle.toLowerCase().includes(query)
                    ).map(product => (
                        <Cards
                            key={product.id}
                            Image={product.image}
                            Title={product.libelle}
                            Price={product.price}
                            Description={product.description}
                        />
                    ))}
                </Row>

            </Container>
        </div>
    );
}

export default Favorite;